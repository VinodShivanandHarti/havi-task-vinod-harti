@extends('layouts.app')

@section('content')

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" />


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>

@if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif

<div class="container">
    <br/>
    <br/>
    <div class="col-md-12">
        <div class="col-md-2" >
            <br/>
            <br/>
            <td><a href="/admin/images"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit">View Thumbnail Images</button></a></td>
        </div>
        <div class="col-md-10">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>LastName</th>
                        <th>Email</th>
                        <th>Gender</th>
                        <th>Admin Notes</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($data as $value)
                    <tr>
                     <td>{{$value->id}}</td>
                     <td>{{$value->name}}</td>
                     <td>{{$value->lastname}}</td>
                     <td>{{$value->email}}</td>
                     <td>{{$value->gender}}</td>
                     <td>{{$value->note}}</td>
                     <td><a href="/admin/form/{{$value->id}}"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit">Add Note</button></a></td>
                     @endforeach
                 </tr> 
             </table>


             <script>
               $(document).ready(function() {
                $('#example').DataTable();
            } );
        </script>

      @endsection