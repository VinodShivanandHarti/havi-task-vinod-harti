@extends('layouts.app')

@section('content')


<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" />


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

@if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif

<div class="container">
    <br/>
    <br/>
    <div class="col-md-12">
        <div class="col-md-2" >
            <br/>
            <br/>
            <td><a href="/admin"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit">View Records</button></a></td>
        </div>
        <div class="col-md-10">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Photo</th>
                        <th>Print</th>
                        <th>Save/ download</th>
                    </tr>
                </thead>
                <tbody>
                  
                   <?php foreach($data as $value){
                    $photo=$value->photo;
                }
                ?>



                @foreach ($data as $value)
                <tr>
                 <td>{{$value->id}}</td>
                 <td>{{$value->name}}</td>
                 <td>{{$value->email}}</td>
                 
                 <td><embed  class="img-thumbnail" alt="" src="{{asset('images/'.$value->photo)}}" alt="Sorry! Image not available at this time" 
                     onError="this.onerror=null;this.src='{{asset('uploads/images/file_not_found.png')}}';"  width="350" height="250" style="width: 200px; height: 150px;"></td>


                     <td><button class="btn btn-primary hidden-print" title="MOU" onclick="window.open('{{asset('images/'.$value->photo)}}', '_blank', 'fullscreen=yes'); return false;">
                        <i class="fa fa-print"></i></button></td>
                        
                        <td><a href="{{url('/admin/download2/'.$value->photo)}}" download="FileName" class="btn" title="MOU"><button type="button" class="btn btn-default btn-sm">
                          <span class="glyphicon glyphicon-download-alt"></span> Download
                      </button></a></td>

                  </div>

                  @endforeach
              </tr> 
          </table>


          <script>
           $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>


     @endsection