<!doctype html>
<html lang="en">
<head>
	<title>
		Form Fillup
	</title>

	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" integrity="sha384-1CmrxMRARb6aLqgBO7yyAxTOQE2AKb9GfXnEo760AUcUmFx3ibVJJAzGytlQcNXd" crossorigin="anonymous"></script>

</head>

<body>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	
	<div class="container">

		@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif

		<form method="post" action="{{route('post_form')}}" enctype="multipart/form-data">
			 {{ csrf_field() }} 
			<div class="form-group">
				<label for="email">Name:</label>
				<input type="text" class="form-control" id="name" placeholder="Enter FirstName" name="name" required="required">
			</div>
			<div class="form-group">
				<label for="email">Last Name:</label>
				<input type="text" class="form-control" id="lastname" placeholder="Enter LastName" name="lastname" required="required">
			</div>
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" class="form-control" id="email" placeholder="Enter email" name="email" required="required">
			</div>
			<div class="form-group">
				<label for="pwd">Password:</label>
				<input type="password" class="form-control" id="password" placeholder="Enter password" name="password" required="required">
			</div>
			<div class="form-group">
				<label for="gender" >Gender:</label>
				<select name="gender" id="gender" class="form-control">
					<option value="male">Male</option>
					<option value="female">Female</option>
				</select>
			</div>
			<div class="form-group">
				<label for="Photo">Photo:</label>
				<input type="file" class="form-control" id="photo"  name="photo" required="required">
			</div>
			<div class="form-group">
				<label for="DOB">Date Of Birth:</label>
				<input type="date" class="form-control" id="date"  name="date" required="required">
			</div>
			<div class="form-group">
				<label for="PDF">PDF File:</label>
				<input type="file" class="form-control" id="pdf"  name="pdf" required="required">
			</div>
			<div class="form-group">
				<label for="SPL">Social Profile Link:</label>
				<input type="text" class="form-control" id="link"  name="link" required="required"/>
			</div>
			
			<div class="form-group">
				<label for="Description">Description:</label>
				<textarea class="form-control" id="desc"  name="desc" required="required"></textarea>
			</div>

			<input type="submit" class="btn btn-primary" name="submit" value="submit"/>
		</div>
	</form>
</body>