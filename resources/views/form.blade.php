
@extends('layouts.app')

@section('content')


<!doctype html>
<html lang="en">
<head>
	<title>
		Form Fillup
	</title>

	
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" integrity="sha384-1CmrxMRARb6aLqgBO7yyAxTOQE2AKb9GfXnEo760AUcUmFx3ibVJJAzGytlQcNXd" crossorigin="anonymous"></script>

</head>

<body>


<div class="container">
@foreach ($data as $value)
    
 <form name="myForm" action="/admin/edit_update/{{ $value->id }}" method="post">
 	@endforeach
 	{{ csrf_field() }} 
<div class="form-group">
	<label>Note</label>
	<textarea class="form-control" name="note" rows="5" id="note"></textarea>
</div>

 <button type="submit" class="btn btn-primary" style="border-radius: 12px;">
                        {{ __('Save Note ') }}
                    </button>

                </form>

                @endsection