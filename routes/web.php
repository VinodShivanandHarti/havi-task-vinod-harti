<?php

Route::get('index', 'userController@index');
Route::get('/', 'userController@iframe');
Route::post('/post_form', 'userController@insertOrUpdate')->name('post_form');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Auth::routes();


Route::prefix('admin')->group(function() {
	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::get('/', 'AdminController@index');
	Route::get('/', 'AdminController@retrive');
	Route::post('/', 'Auth\AdminLoginController@update')->name('update');

	Route::get('/form/{id}', 'AdminController@update');
	Route::post('/edit_update/{id}', 'AdminController@edit_update');
	Route::get('/images', 'AdminController@image');
	Route::get('/download2/{photo}', 'AdminController@photo');
});