<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Form;
use DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('/');
    }

    public function retrive(){

        $data = Form::all();

        return view('admin', compact('data'));
    }

    public function update($id){

        $data = Form::where('id', '=', $id)->get();
        
        return view('form', compact('data'));
    }


    public function edit_update(Request $request, $id)
    {

        DB::table('form_data')
        ->where('id', $id)
        ->update(['note' => $request->note]);

        $data = Form::all();

        return view('admin', compact('data'))->with('message', 'Success!');

    }

    public function image(){
        $data = Form::all();

        return view('/images', compact('data'));
    }

    public function photo($photo)
    {
        $file = public_path() . "/images/$photo";
        $headers = array(
            'Content-Type: image/jpeg',
        );
        return response()->download($file, $photo, $headers);
    }

}
