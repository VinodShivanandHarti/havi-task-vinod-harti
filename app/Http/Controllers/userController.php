<?php

namespace App\Http\Controllers;

use App\Model\Form;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class userController extends Controller
{
	public function index(){
		return view('index');
	}

	public function iframe(){
		return view('iframe');
	}

	public function insertOrUpdate(Request $request){

		$this->validate($request, [
			'photo' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
			"pdf" => "required|mimes:pdf|max:10000"
		]);

		$model = Form::firstOrNew(array('email' => $request->input('email')));

		if($request->input('name')) $model->name = $request->input('name');
		if($request->input('lastname')) $model->lastname = $request->input('lastname');
		if($request->input('email')) $model->email = $request->input('email');
		if($request->input('password')) $model->password = Hash::make($request->input('password'));
		if($request->input('gender')) $model->gender = $request->input('gender');
		if($request->input('date')) $model->date = $request->input('date');
		if($request->input('link')) $model->link = $request->input('link');
		if($request->input('desc')) $model->desc = $request->input('desc');
		if($request->input('note')) {
			$model->note = $request->input('note');
		}

		if ($request->hasfile('photo')) { $filename = time().'.'.request()->photo->getClientOriginalExtension();
		request()->photo->move(public_path('images'), $filename);
		$model->photo=$filename;
	}

if ($request->hasfile('pdf')) {
		$filename = time().'.'.request()->pdf->getClientOriginalExtension();
		request()->pdf->move(public_path('files'), $filename);
		$model->pdf=$filename;
	}

		$model->save();

		return redirect()->back()->with('success', 'Inserted Successfully');




                
	}
}
