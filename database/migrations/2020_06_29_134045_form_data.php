<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FormData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('form_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 15);
            $table->string('lastname', 15);
            $table->string('email', 50)->unique();
            $table->string('password', 191);
            $table->string('gender', 10);
            $table->string('photo');
            $table->date('date');
            $table->string('pdf');
            $table->string('link');
            $table->string('desc');
            $table->string('note')->nullable($value = true);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     Schema::dropIfExists('form_data');
    }
}
